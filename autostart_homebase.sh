#!/bin/bash
echo "start"
lxterminal -t "Sound patterns starting" --command="/bin/bash --init-file /home/pi/pi2pi/autostart_sound.sh"
lxterminal -t "Chromium starting (seeder)" --command="/bin/bash --init-file /home/pi/pi2pi/autostart_chromium.sh"
lxterminal -t "IPFS daemon" --command="/bin/bash --init-file /home/pi/pi2pi/ipfs_daemon.sh"
homebase --config /home/pi/pi2pi/homebase.yml
