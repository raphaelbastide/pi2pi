# Raspberry pi as a P2P web server / seeder (IPFS + DAT)

## Requirements

- Fist, make sure SSH access is enabled in the pi’s config panel, so you can access you pi from another computer in the network
- Then, you might want to remove unused packages installed on a Raspbian pi by default
 - `$ sudo apt-get remove --purge wolfram-engine bluej nodered nuscratch scratch sonic-pi libreoffice* claws-mail claws-mail-i18n minecraft-pi python-pygame && sudo apt-get autoremove --purge && sudo apt-get update && sudo apt-get upgrade -y`
- Install Nodejs and npm for the pi:
 - `$ curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash`
 - `$ sudo apt install -y nodejs`
- Install ipfs ([guide](https://docs.ipfs.io/guides/guides/install/)) (Chose ARM for the pi x)
- Install homebase ([guide](https://github.com/beakerbrowser/homebase#install))
- Install wiringPi ([guide](http://wiringpi.com/download-and-install/))
- Chromium should be already installed on the Pi
- Optional: [Beaker Browser](http://beakerbrowser.com/) ([guide below](#install-beaker-browser-on-the-raspberrypi-pi-optional))

## Installation

I suggest to `git clone` this repo at the root of the Raspberry Pi. By default, this will create a copy of this repo in `~/pi2pi`.

`git clone https://gitlab.com/raphaelbastide/pi2pi.git`

### Auto start the fist terminal window

You need to modify `~/.config/lxsession/LXDE-pi/autostart`. If it doesn’t exist, see below.

```~/.config/lxsession/LXDE-pi/autostart
@lxpanel --profile LXDE-pi
@pcmanfm --desktop --profile LXDE-pi
# @xscreensaver -no-splash
# @point-rpi

@xset s off
@xset -dpms
@bash /home/pi/pi2pi/autostart.sh
```

### If autostart does not exist

You need to copy the content of the existing autostart to a new one in the user folder:

```
$ mkdir ~/.config/lxsession/ && mkdir ~/.config/lxsession/LXDE-pi && cp /etc/xdg/lxsession/LXDE-pi/autostart ~/.config/lxsession/LXDE-pi/autostart
```

## IPFS

Once Ipfs is installed, you can quiclky run a node with:

`$ mkdir ipfs && cd ipfs && ipfs init`

### Boot

When rasberry pi boots, the applications bellow automatically run

- homebase
- wiringPi
- chromium
- IPFS node

## Install Beaker Browser on the Raspberrypi Pi (optional)

`sudo apt-get install libtool m4 make g++`

I also had to install autoconf to avoid an issue with this dependency:

`sudo apt-get install autoconf`

Then clone Beaker (you may have to install git first)

`git clone https://github.com/beakerbrowser/beaker.git`

`cd beaker`

Checkout to v. 0.8.2 to avoid the [electron related bug](https://github.com/electron/electron/issues/16205):

`git checkout 0.8.2`
Install, rebuild

`npm install`

`npm run rebuild`

Try `npm start`, if stuck at `[ dat-daemon ] Starting…`, try:

`npm start > /dev/null` (Thanks to https://github.com/beakerbrowser/beaker/issues/1270#issuecomment-445958546)

# References
- [wiringPi](http://wiringpi.com/)
- [homebase](https://github.com/beakerbrowser/homebase)
- Vogelhaus documentation page dat://d0cdd1916150c0e9a6b6b1c66df16a2c5808c1135952d7bed925c4146eff7744/
- [Autostart for Raspberry pi](https://github.com/kiyu-git/autostart-for-rasberry-pi)
